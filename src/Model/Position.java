package Model;

public class Position {
    private final int coordinateX;
    private final int coordinateY;
    private final int coordinateZ;
    private final int creationId;

    public Position(int coordinateX, int coordinateY, int coordinateZ, int creationId) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.coordinateZ = coordinateZ;
        this.creationId = creationId;
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public int getCoordinateZ() {
        return coordinateZ;
    }

    public int getCreationId() {
        return creationId;
    }

    @Override
    public String toString() {
        return "Position{" +
                "coordinateX=" + coordinateX +
                ", coordinateY=" + coordinateY +
                ", coordinateZ=" + coordinateZ +
                ", creationId=" + creationId +
                '}';
    }
}
