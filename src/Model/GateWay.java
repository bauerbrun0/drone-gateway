package Model;

import Model.System.PositionReceiver;
import View.Output;

public class GateWay {
    private Buffer buffer = new Buffer();
    private NetworkStorage networkStorage;

    public GateWay(NetworkStorage networkStorage, Output output){
        this.networkStorage = networkStorage;
    }

    public void receivePosition(Position position){
        ReceivedPosition receivedPosition = PositionReceiver.receive(position);
        if(networkStorage.isOnline()){
            if(bufferIsNotEmpty()) synchronize();
            networkStorage.addPosition(receivedPosition);
        } else {
            buffer.addPosition(receivedPosition);
        }

    }

    private boolean bufferIsNotEmpty(){
        if(buffer.getNumberOfPositions() > 0) return false;
        else return true;
    }

    public Buffer getBuffer(){
        return buffer;
    }

    public void removeFromBuffer(int creationId){
        buffer.delete(creationId);
    }

    public boolean storageIsOnline(){
        return networkStorage.isOnline();
    }

    public void setStorageToOnline(){
        networkStorage.setToOnline();
        synchronize();
    }

    public void setStorageToOffline(){
        networkStorage.setToOffline();
    }

    public void deleteLastPositionsFromBuffer(int numberOfPositionsToDelete){
        for(int i = 1; i <= numberOfPositionsToDelete; i++){
            buffer.deleteLastPosition();
        }
    }

    public void synchronize(){
        networkStorage.synchronize(buffer);
        buffer.clear();
    }
}
