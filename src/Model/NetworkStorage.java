package Model;

import View.Output;

import java.util.ArrayList;

public class NetworkStorage {
    private ArrayList<ReceivedPosition> storage = new ArrayList<>();
    private boolean isOnline = true;
    private Output output;

    public NetworkStorage(Output output){
        this.output = output;
    }

    public void addPosition(ReceivedPosition position){
        storage.add(position);
    }

    public void synchronize(Buffer buffer){
        for (int i = 0; i < buffer.getNumberOfPositions(); i++){
            addPosition(buffer.getPosition(i));
        }
    }

    public boolean isOnline(){
        return isOnline;
    }

    private void changeAvailability(boolean value){
        isOnline = value;
    }

    public void setToOffline(){
        changeAvailability(false);
    }

    public void setToOnline(){
        changeAvailability(true);
    }
}
