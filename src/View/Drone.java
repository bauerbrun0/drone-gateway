package View;

import Model.Position;

public interface Drone {
    Position sendPosition();
}
