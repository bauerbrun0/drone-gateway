package View;

import Model.Position;

public class ConsoleDrone implements Drone{
    Input input;

    public ConsoleDrone(Input input){
        this.input = input;
    }


    @Override
    public Position sendPosition() {
        return getPositionFromConsole();
    }

    private Position getPositionFromConsole(){
        return new Position(
                input.getInt("X"),
                input.getInt("Y"),
                input.getInt("Z"),
                input.getInt("creation")
        );
    }
}
