package View;

public interface Input {
    int getInt(String message);

    String getString(String message);

}
